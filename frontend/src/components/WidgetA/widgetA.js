import React from "react";
import './style.scss';

const WidgetA = () => (
    <div className="widget">
        <header>
            <h1 className="widget__title">Widget Title</h1>
            <h3 className="widget__subtitle">Widget Subtitle</h3>
        </header>
        <main className="widget__content">
            <ul>
                <li>ABC</li>
                <li>DEF</li>
                <li>GHI</li>
                <li>KJL</li>
                <li>MNO</li>
            </ul>
        </main>
    </div>
);

export default WidgetA;