import React from "react";
import WidgetA from "./widgetA";
import renderer from 'react-test-renderer';

it('Snapshot renders correctly', () => {
    const tree = renderer
        .create(<WidgetA />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});