import React from "react";
import WidgetB from "./widgetB";
import renderer from 'react-test-renderer';

it('Snapshot renders correctly', () => {
    const tree = renderer
        .create(<WidgetB />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});