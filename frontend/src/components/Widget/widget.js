import React from "react";
import CountUp from "react-countup";
import './style.scss';

/**
 *
 * @param header
 * @param footerLeft
 * @param footerRight
 * @param content
 * @returns {*}
 * @constructor
 */
const Widget = ({ header, footerLeft, footerRight, content})=> (
    <div className="widget widget--default">
        <header>
            <h1 className="widget__title">{header.title}</h1>
            <h3 className="widget__subtitle">{header.subtitle}</h3>
        </header>

        {content && content.value && content.suffix &&
            <main className="widget__content">
                <CountUp
                    start={0}
                    end={content.value}
                    suffix={content.suffix}
                />
            </main>
        }

        <footer className="widget__footer">
            <div>
                <h3 className="widget__subtitle">{footerLeft.title}</h3>
                <p>{footerLeft.content}</p>
            </div>
            <div>
                <h3 className="widget__subtitle">{footerRight.title}</h3>
                <p>{footerRight.content}</p>
            </div>
        </footer>
    </div>
)

export default Widget;