import React from "react";
import Widget from './widget';
import renderer from 'react-test-renderer';

it('Snapshot renders correctly', () => {
    const tree = renderer
        .create(<Widget
            header={{ title: 'Support Issues', subtitle: 'Last 7 days'}}
            footerLeft={{ title: '', content: ''}}
            footerRight={{ title: '', content: ''}}
            content={{
                value: 20,
                suffix: ' Issues'
            }}
        />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});