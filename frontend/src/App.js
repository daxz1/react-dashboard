import React from 'react';
import {
    Widget,
    WidgetA,
    WidgetB
} from "./components";

const App = () => (
    <div className="dashboard">
        <div className='dashboard__container'>
            <WidgetA />
            <WidgetB />
            <WidgetA />
        </div>
        <div className='dashboard__container'>
            <Widget
                header={{ title: 'Sales', subtitle: 'IT Dept'}}
                footerLeft={{ title: 'Top Contributor', content: 'Pete'}}
                footerRight={{ title: '% Income', content: '20%'}}
                content={{
                    value: 20,
                    suffix: '%'
                }}
            />
            <Widget
                header={{ title: 'Sales', subtitle: 'Biggest Sale'}}
                footerLeft={{ title: 'Top Salesperson', content: 'John Doe'}}
                footerRight={{}}
                content={{
                    value: 200,
                    suffix: 'k'
                }}
            />
            <Widget
                header={{ title: 'Support Issues', subtitle: 'Last 7 days'}}
                footerLeft={{ title: '', content: ''}}
                footerRight={{ title: '', content: ''}}
                content={{
                    value: 20,
                    suffix: ' Issues'
                }}
            />
            <Widget
                header={{ title: 'Open JIRA Tickets', subtitle: 'Sprint 01/01/2020'}}
                footerLeft={{ title: '', content: ''}}
                footerRight={{ title: '', content: ''}}
                content={{}}
            />
        </div>
    </div>
);

export default App;
