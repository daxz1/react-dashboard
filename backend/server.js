import express from 'express';
import fetch from 'node-fetch';

const app = express();
const PORT = 3001;

let cache = [];

/**
 * Check if ID exists in cache
 * @param id
 * @returns {boolean}
 */
const checkCache = (id) => (!!cache[id]);

/**
 * Fetch Superhero data from API.
 * @param id
 * @returns {Promise<any>}
 */
const fetchSuperHero = async (id) => {
    const response  = await fetch(`https://superheroapi.com/api/3127088004051555/${id}`);
    return await response.json();
}

/**
 *
 * @param response
 * @returns {boolean}
 */
const checkResponseIsSuccess = ({response}) => (response === 'success');


app.get('/search/:id', async (req, res) => {
    const { id } = req.params;

    if (checkCache(id)) {
        return res.send(cache[id]);
    }

    const results = await fetchSuperHero(id);

    if (checkResponseIsSuccess(results)) {
        cache[id] = results;
        return res.send(results);
    }
    // Else failure
    return res.send({});
});

app.listen(PORT, () => {
    console.log(`BACKEND LISTENING ON ${PORT}`);
})